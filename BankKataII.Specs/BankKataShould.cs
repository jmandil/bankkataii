using NUnit.Framework;
using Shouldly;

namespace BankKataII.Specs
{
    [TestFixture]
    public class BankKataShould
    {
        [Test]
        public void PrintStatementWithTransactions()
        {
            var account = new Account();
            var teller = new Teller(account);

            teller.Deposit(100);
            teller.Withdraw(15);

            var result = teller.PrintStatement();

            result.Transactions.Count.ShouldBe(2);
            (result.Transactions[1].DateTime < result.Transactions[0].DateTime).ShouldBeTrue();
            result.Transactions[0].Amount.ShouldBe(-15);
            result.Transactions[1].Amount.ShouldBe(100);

            result.Balance.ShouldBe(85);
        }
    }
}