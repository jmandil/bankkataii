﻿using System;
using NUnit.Framework;
using Shouldly;

namespace BankKataII.Tests
{
    [TestFixture]
    public class AccountShould
    {
        [Test]
        public void RetrieveTransactionsWhenNoDepositsOrWithdrawalsHaveBeenMade()
        {
            var account = new Account();

            var result = account.RetrieveTransactions();

            result.Count.ShouldBe(0);
        }

        [Test]
        public void RetrieveTransactionsWhenMultipleDepositsHaveBeenMade()
        {
            var account = new Account();
            var dateTime1 = new DateTime(2017, 1, 1, 13, 15, 00);
            var dateTime2 = new DateTime(2017, 1, 1, 13, 20, 00);
            account.Deposit(100, dateTime1);
            account.Deposit(50, dateTime2);

            var result = account.RetrieveTransactions();

            result.Count.ShouldBe(2);
            result[0].Amount.ShouldBe(100);
            result[0].DateTime.ShouldBe(dateTime1);
            result[1].Amount.ShouldBe(50);
            result[1].DateTime.ShouldBe(dateTime2);
        }

        [Test]
        public void MakeAWithdrawal()
        {
            var account = new Account();
            var dateTime1 = new DateTime(2017, 1, 1, 13, 15, 00);
            var dateTime2 = new DateTime(2017, 1, 1, 13, 20, 00);
            account.Deposit(100, dateTime1);
            account.Withdraw(50, dateTime2);

            var result = account.RetrieveTransactions();

            result.Count.ShouldBe(2);
            result[0].Amount.ShouldBe(100);
            result[0].DateTime.ShouldBe(dateTime1);
            result[1].Amount.ShouldBe(-50);
            result[1].DateTime.ShouldBe(dateTime2);
        }
    }
}
