﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Shouldly;

namespace BankKataII.Tests
{
    [TestFixture]
    public class TellerShould
    {
        [Test]
        public void DepositIntoAccount()
        {
            var mockAccount = new Mock<IAccount>();
            var teller = new Teller(mockAccount.Object);
            const int amountToDeposit = 100;
            
            teller.Deposit(amountToDeposit);

            mockAccount.Verify(a => a.Deposit(amountToDeposit, It.IsAny<DateTime>()), Times.Once);
        }

        [Test]
        public void WithdrawFromAccount()
        {
            var mockAccount = new Mock<IAccount>();
            var teller = new Teller(mockAccount.Object);
            const int amountToWithdraw = 30;
            
            teller.Withdraw(amountToWithdraw);

            mockAccount.Verify(a => a.Withdraw(amountToWithdraw, It.IsAny<DateTime>()), Times.Once);
        }

        [Test]
        public void GetTransactionHistoryOfAccount()
        {
            var mockAccount = new Mock<IAccount>();
            var teller = new Teller(mockAccount.Object);
            var transactions = new List<Transaction>
            {
                new Transaction(100, new DateTime(2017, 1, 1, 12, 00, 00)),
                new Transaction(-10, new DateTime(2017, 1, 1, 12, 10, 00)),
                new Transaction(5, new DateTime(2017, 1, 1, 12, 13, 00)),
            };
            mockAccount.Setup(a => a.RetrieveTransactions()).Returns(transactions);

            var result = teller.PrintStatement();

            (
                result.Transactions[0].DateTime > result.Transactions[1].DateTime &&
                result.Transactions[1].DateTime > result.Transactions[2].DateTime 
            ).ShouldBeTrue();
        }
    }
}
