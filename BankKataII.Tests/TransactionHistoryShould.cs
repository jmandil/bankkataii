﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Shouldly;

namespace BankKataII.Tests
{
    [TestFixture]
    public class TransactionHistoryShould
    {
        [Test]
        public void ReturnBalanceOfTransactionHistory()
        {
            var transactionhistory = new TransactionHistory
            {
                Transactions = new List<Transaction>
                {
                    new Transaction(200, new DateTime(2017, 1, 1, 15, 00, 00)),
                    new Transaction(-20, new DateTime(2017, 1, 1, 15, 10, 00)),
                    new Transaction(30, new DateTime(2017, 1, 1, 15, 50, 00)),
                    new Transaction(-100, new DateTime(2017, 1, 1, 16, 00, 00))
                }
            };

            transactionhistory.Balance.ShouldBe(110);
        }
    }
}
