﻿using System;
using NUnit.Framework;
using Shouldly;

namespace BankKataII.Tests
{
    [TestFixture]
    public class TransactionShould
    {
        [Test]
        public void ReturnAmountAndDateForNewTransaction()
        {
            var dateTime = new DateTime(2017,1,1,13,10,00);

            var transaction = new Transaction(200, dateTime);

            transaction.Amount.ShouldBe(200);
            transaction.DateTime.ShouldBe(dateTime);
        }
    }
}
