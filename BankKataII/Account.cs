using System;
using System.Collections.Generic;

namespace BankKataII
{
    public interface IAccount
    {
        List<Transaction> RetrieveTransactions();
        void Deposit(int amount, DateTime dateTime);
        void Withdraw(int amount, DateTime dateTime);
    }

    public class Account : IAccount
    {
        private readonly List<Transaction> _transactions;

        public Account()
        {
            _transactions = new List<Transaction>();
        }

        public List<Transaction> RetrieveTransactions()
        {
            return _transactions;
        }

        public void Deposit(int amount, DateTime dateTime)
        {
            _transactions.Add(new Transaction(amount, dateTime));
        }

        public void Withdraw(int amount, DateTime dateTime)
        {
            _transactions.Add(new Transaction(-amount, dateTime));
        }
    }
}