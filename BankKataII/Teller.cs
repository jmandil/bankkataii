using System;
using System.Linq;

namespace BankKataII
{
    public class Teller
    {
        private readonly IAccount _account;

        public Teller(IAccount account)
        {
            _account = account;
        }
        
        public TransactionHistory PrintStatement()
        {
            var transactions = _account.RetrieveTransactions().OrderByDescending(o => o.DateTime).ToList();

            return new TransactionHistory {Transactions = transactions };
        }

        public void Deposit(int amount)
        {
            _account.Deposit(amount, DateTime.Now);
        }

        public void Withdraw(int amount)
        {
            _account.Withdraw(amount, DateTime.Now);
        }
    }
}