using System;

namespace BankKataII
{
    public class Transaction
    {
        public DateTime DateTime { get; private set; }
        public int Amount { get; private set; }

        public Transaction(int amount, DateTime dateTime)
        {
            Amount = amount;
            DateTime = dateTime;
        }
    }
}