using System.Collections.Generic;
using System.Linq;

namespace BankKataII
{
    public class TransactionHistory
    {
        public IList<Transaction> Transactions { get; set; }

        public int Balance
        {
            get { return Transactions.Select(x => x.Amount).Sum(); }    
        }
    }
}