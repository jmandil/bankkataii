# README #

### What is this repository for? ###

* Personal Bank Kata project to practice TDD
* 0.0.0.1

### How do I get set up? ###

* .Net class library
* NuGet Packages: NUnit, Moq, Shouldy
* NUnit Runner or ReSharper

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Owner - Jasen
